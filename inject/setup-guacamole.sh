#!/bin/bash -x

source /host/settings.sh

# if DB settings are not defined, use some default values
DBHOST=${DBHOST:-localhost}
DBPORT=${DBPORT:-3306}
DBNAME=${DBNAME:-${DOMAIN//./_}}
DBNAME=${DBNAME:-guacamole_db}
DBUSER=${DBUSER:-${DOMAIN//./_}}
DBUSER=${DBUSER:-guacamole}
DBPASS=${DBPASS:-$(tr -cd '[:alnum:]' < /dev/urandom | fold -w16 | head -n1)}

# set the options of the mariadb command
mariadb="mariadb -h $DBHOST -P $DBPORT -D $DBNAME -u $DBUSER --password=$DBPASS"

main() {
    setup_db
    init_db_schema
    setup_guacamole_properties
    setup_recordings
    modify_tomcat9_config
    systemctl restart tomcat9
}

setup_db() {
    if [[ $DBHOST != 'localhost' ]]; then
        systemctl stop mariadb
        systemctl disable mariadb
        systemctl mask mariadb
    else
        # create the database and user
        mariadb -e "
            DROP DATABASE IF EXISTS $DBNAME;
            CREATE DATABASE $DBNAME;
            DROP USER IF EXISTS '$DBUSER'@'localhost';
            CREATE USER '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASS';
            GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBUSER'@'localhost';
            FLUSH PRIVILEGES;
        "
        _fix_mariadb_service_bug
    fi
}

_fix_mariadb_service_bug() {
    # fix a bug with systemd
    # see: https://jira.mariadb.org/browse/MDEV-23050
    sed -i /lib/systemd/system/mariadb.service \
        -e '/SendSIGKILL/ c SendSIGKILL=yes'
    systemctl daemon-reload
    systemctl restart mariadb
}

init_db_schema() {
    # create the db tables
    cat /etc/guacamole/001-create-schema.sql | $mariadb

    # create an admin user, if it does not exist
    nr=$($mariadb -sN -e 'SELECT COUNT(*) FROM guacamole_entity')
    if [[ $nr == '0' ]]; then
        cat /etc/guacamole/002-create-admin-user.sql | $mariadb
    fi

    # set the username and password of the admin user to $ADMIN and $PASS
    [[ -n $PASS ]] && $mariadb -e "
        SET @salt = UNHEX(SHA2(UUID(), 256));

        UPDATE guacamole_user
        SET password_salt = @salt,
            password_hash = UNHEX(SHA2(CONCAT('$PASS', HEX(@salt)), 256))
        WHERE user_id = 1;

        UPDATE guacamole_entity
        SET name = '$ADMIN'
        WHERE entity_id = 1 AND type = 'USER';
    "

    # cleanup the mariadb init scripts
    rm /etc/guacamole/00*.sql
}

setup_guacamole_properties() {
    mkdir -p /etc/guacamole
    cat <<EOF > /etc/guacamole/guacamole.properties
guacd-hostname: 127.0.0.1
guacd-port:     4822

mysql-driver:   mariadb
mysql-ssl-mode: disabled
mysql-hostname: $DBHOST
mysql-port:     $DBPORT
mysql-database: $DBNAME
mysql-username: $DBUSER
mysql-password: $DBPASS
mysql-default-max-connections: ${MAX_CONNECTIONS:-50}
mysql-default-max-connections-per-user: ${MAX_CONNECTIONS:-50}
EOF
    cat <<EOF > /etc/guacamole/guacd.conf
[server]
bind_host = 127.0.0.1
bind_port = 4822
EOF

    # link to tomcat libraries
    ln -sf /etc/guacamole/guacamole.properties \
       /usr/share/tomcat9/lib/guacamole.properties
}

setup_recordings() {
    chown daemon:tomcat -R /var/lib/guacamole/recordings
    chmod 2750 /var/lib/guacamole/recordings
}

modify_tomcat9_config() {
    local config_file=/var/lib/tomcat9/conf/server.xml

    # see: https://guacamole.apache.org/doc/gug/reverse-proxy.html#preparing-your-servlet-container
    xmlstarlet edit -P -S -L \
               -a /Server/Service/Connector -t attr -n URIEncoding -v UTF-8 \
               $config_file

    # see: https://guacamole.apache.org/doc/gug/reverse-proxy.html#setting-up-the-remote-ip-valve
    xmlstarlet edit -P -S -L \
               -s //Host -t elem -n ValveTMP -v '' \
               -i //ValveTMP -t attr -n className -v org.apache.catalina.valves.RemoteIpValve \
               -i //ValveTMP -t attr -n internalProxies -v 127.0.0.1 \
               -i //ValveTMP -t attr -n remoteIpHeader -v x-forwarded-for \
               -i //ValveTMP -t attr -n remoteIpProxiesHeader -v x-forwarded-for \
               -i //ValveTMP -t attr -n protocolHeader -v x-forwarded-proto \
               -r //ValveTMP -v Valve \
               $config_file
}

# call main()
main "$@"
