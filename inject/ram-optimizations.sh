#!/bin/bash -x

# tomcat
sed -i /etc/default/tomcat9 \
    -e '/^JAVA_OPTS/ a JAVA_OPTS="${JAVA_OPTS} -Xmx20m -Xms20m"'
systemctl restart tomcat9
