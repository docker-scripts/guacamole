#!/bin/bash -x

# based on: https://guacamole.apache.org/doc/gug/reverse-proxy.html
ip=$(hostname -I)
network=${ip%.*}.0/16
cat <<EOF > /etc/nginx/sites-available/default
# vim: syntax=nginx
server {
    listen 443 ssl;
    server_name _;

    ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
    ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;

    real_ip_header X-Forwarded-For;
    set_real_ip_from $network;

    location / {
        proxy_pass http://localhost:8080/guacamole/;
        proxy_buffering off;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$http_connection;

        location /tunnel/ {
            access_log off;
        }
    }
}
EOF

systemctl reload nginx
