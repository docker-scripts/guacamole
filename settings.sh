APP="guacamole"
IMAGE="dockerscripts/guacamole"
DOMAIN="guac.example.org"

### If not behind 'revproxy', expose the port 443.
### In this case the DOMAIN should be commented
### because it is related to 'revproxy'.
#PORTS="443:443"

############################################################################

### Guacamole admin user.
### For security reasons change at least the PASS.
ADMIN="admin"
PASS="pass"

### Default maximum connections for each server.
#MAX_CONNECTIONS="50"

############################################################################

### Uncomment DB settings to use an external MariaDB database.
### You must have a server running MariaDB, for example install 'mariadb'
### container with docker-scripts: https://gitlab.com/docker-scripts/mariadb
#DBHOST=mariadb    # this is the name of the mariadb container
#DBPORT=3306
#DBNAME=${DOMAIN//./_}
#DBUSER=${DOMAIN//./_}
#DBPASS=123456
