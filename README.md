# Guacamole In a Container

## Installation

- First install `ds` and `revproxy`:
   - https://gitlab.com/docker-scripts/ds#installation
   - https://gitlab.com/docker-scripts/revproxy#installation

- Then get the scripts: `ds pull guacamole`

- Create a dir for the container: `ds init guacamole @guac.example.org`

- Fix the settings: `cd /var/ds/guac.example.org/; vim settings.sh`
  Make sure to change the admin password.

- Make the container: `ds make`

- Open https://guac.example.org/

Related links:

- [Presentation and demo at SFScon 2021](https://www.youtube.com/watch?v=EUJHeHGpjb0&t=2700s)
- http://dashohoxha.fs.al/virtual-computer-lab/
- [Presentation at FOSSCOM 2020](https://www.youtube.com/watch?v=CIj_6U2cD0I)
- [Interactive tutorial on katacoda](https://katacoda.com/dashohoxha/courses/docker-scripts/virtual-computer-lab)


## Configuration

First you need to install one or more Linux servers:
  - https://gitlab.com/docker-scripts/linuxmint#installation 

Then you can create connections and users like this:

```bash
ds guac

ds guac conn add linuxmint-1 ssh
ds guac conn add linuxmint-1 rdp
ds guac conn ls

ds guac user add username p@ssw0rd

ds guac user connect username linuxmint-1:ssh
ds guac user connect username linuxmint-1:rdp

ds guac user ls
ds guac user ls username

ds guac user disconnect username linuxmint-1:ssh
```

In this case `linuxmint-1` is the name of the container where the
Linux server is installed, or its domain, or its IP.

### Keyboard layout

If the keyboard of the clients has a layout that is different from the
English one (for example Italian), you should set this as a parameter
to the RDP connection. For more details see the parameter
`server-layout` at
https://guacamole.apache.org/doc/gug/configuring-guacamole.html#rdp

With `ds guac` you can set the keyboard layout like this:

```bash
ds guac conn add linuxmint-1 rdp it-it-qwerty
ds guac conn add linuxmint-1 rdp de-de-qwertz

ds guac user connect user1 linuxmint-1:rdp:it
ds guac user connect user1 linuxmint-1:rdp:de
```

In this example we are creating two RDP connections to the same server
(`linuxmint-1`), one with an Italian keyboard layout and one with a
German layout, and we are allowing `user1` to use these
connections. If the clients that are connected through `user1` have an
Italian keyboard, they can use the first connection; if they have a
German one, they can use the second connection.

## Using an external DB

If these lines on `settings.sh` are uncommented, guacamole will use an
external DB:

```bash
DBHOST=mariadb    # this is the name of the mariadb container
DBPORT=3306
DBNAME=guac_example_org
DBUSER=guac_example_org
DBPASS=123456
```

Using an external DB has the advantage that the configurations persist
when you remake the container.

## Accessing in a LAN

If the guacamole server is in a LAN (for example in a laptop) it has
to be accessed by its IP (like this: https://123.45.67.89/). In this
case the `revproxy` container is not neccessary. To access the
guacamole server in this case you have to uncomment the line
`PORTS="443:433"` on `settings.sh` (and maybe forward another port if
needed, like `PORTS="444:443"`). This has to be done before `ds make`.

Also, make sure to comment out `DOMAIN`.
