cmd_remake_help() {
    cat <<_EOF
    remake
        Rebuild the container, preserving the existing data.

_EOF
}

cmd_remake() {
    set -x
    ds backup
    ds remove
    ds make
    ds restore "$(ls backup-*.tgz | tail -1)"
}
