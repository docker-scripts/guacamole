cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

fail() { echo "$@" >&2; exit 1; }

cmd_config() {
    # check settings
    [[ $PASS == 'pass' ]] \
	&& fail "Error: PASS on 'settings.sh' has to be changed for security reasons."

    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)

    # create the mariadb database
    [[ -n $DBHOST ]] && ds mariadb create

    # setup guacamole
    ds inject setup-guacamole.sh
    ds inject setup-nginx.sh

    #ds inject ram-optimizations.sh
}
