cmd_guac_help() {
    cat <<_EOF
    guac [-d] [<command>] [<arguments>]
        Simple command to manage guacamole connections and users.
        It can be used like this:

        - conn add <server> ssh
        - conn add <server> rdp [<kbd-layout>] [--guest] [--record]
        - conn (list | ls)
        - conn (remove | rm) <connection>

        - user add <username> <password>
        - user (list | ls) [<username>]
        - user (remove | rm) <username>
        - user connect <username> <connection>
        - user disconnect <username> <connection>

        - guest add <connection> <username> [<password>]
        - guest (remove | rm) <connection> [<username>]
        - guest (list | ls) [<connection>]

    With the option '-d' (for debug) it also shows the SQL commands
    that are executed.

_EOF
}

cmd_guac() {
    if [[ $1 == '-d' ]]; then
        cmd_guac_debug=true
        shift
    fi

    local cmd=$1
    shift
    case $cmd in
        conn)
            _guac_conn "$@" ;;
        user)
            _guac_user "$@" ;;
        guest)
            _guac_guest "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_fail() {
    fail "Usage:\n$(cmd_guac_help)"
}

_mariadb() {
    if [[ -n $cmd_guac_debug ]]; then
        echo -e "$*" | >&2 highlight --syntax=sql -O xterm256
    fi
    local dbname=${DOMAIN//./_}
    dbname=${dbname:-guacamole_db}
    local mariadb="mariadb $dbname"
    [[ -n $DBHOST ]] && mariadb="mariadb -h $DBHOST -P $DBPORT -D $DBNAME -u $DBUSER --password=$DBPASS"
    echo -e "$*" | docker exec -i $CONTAINER $mariadb -sN
}

_guac_conn() {
    local cmd=$1
    shift
    case $cmd in
        add)
            _guac_conn_add "$@" ;;
        list|ls)
            _guac_conn_list "$@" ;;
        remove|rm)
            _guac_conn_remove "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_user() {
    local cmd=$1
    shift
    case $cmd in
        add)
            _guac_user_add "$@" ;;
        list|ls)
            _guac_user_list "$@" ;;
        remove|rm)
            _guac_user_remove "$@" ;;
        connect)
            _guac_user_connect "$@" ;;
        disconnect)
            _guac_user_disconnect "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_guest() {
    local cmd=$1
    shift
    case $cmd in
        add)
            _guac_guest_add "$@" ;;
        list|ls)
            _guac_guest_list "$@" ;;
        remove|rm)
            _guac_guest_remove "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_conn_add() {
    # get the arguments
    local server=$1
    [[ -z $server ]] && _guac_fail
    local protocol=$2
    [[ -z $protocol ]] && _guac_fail

    local kbd_layout guest record
    while [[ -n $3 ]]; do
        case $3 in
            --guest)
                guest=true
                shift
                ;;
            --record)
                record=true
                shift
                ;;
            *)
                kbd_layout=$3
                shift
                ;;
        esac
    done

    # create a connection name
    local conn="$server:$protocol"
    [[ -n $guest ]] && conn+=":guest"
    [[ -n $kbd_layout ]] && conn+=":${kbd_layout//-*}"

    # check that it does not already exist
    local id=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$conn' AND protocol = '$protocol';
    ")
    [[ -n $id ]] && fail "Connection already exists"

    # create a guacamole connection
    if [[ -n $guest ]]; then
        _mariadb "
            INSERT INTO guacamole_connection (connection_name, protocol, max_connections_per_user)
            VALUES ('$conn', 'rdp', 1);
            "
    else
        _mariadb "
            INSERT INTO guacamole_connection (connection_name, protocol)
            VALUES ('$conn', '$protocol');
            "
    fi

    # set some parameters for the connection
    local id=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$conn';
    ")
    _mariadb "
        INSERT INTO guacamole_connection_parameter
        VALUES ($id, 'hostname', '$server');
    "
    [[ $protocol == 'ssh' ]] && _mariadb "
        INSERT INTO guacamole_connection_parameter
        VALUES ($id, 'font-name', 'Monaco');
    "
    [[ -n $kbd_layout ]] && _mariadb "
        INSERT INTO guacamole_connection_parameter
        VALUES ($id, 'server-layout', '$kbd_layout');
    "
    [[ -n $guest ]] && _mariadb "
        INSERT INTO guacamole_connection_parameter
        VALUES
            ($id, 'username', '\${GUAC_USERNAME}'),
            ($id, 'password', '\${GUAC_PASSWORD}');
    "
    [[ -n $record ]] && _mariadb "
        INSERT INTO guacamole_connection_parameter
        VALUES
            ($id, 'recording-path', '\${HISTORY_PATH}/\${HISTORY_UUID}'),
            ($id, 'create-recording-path', 'true'),
            ($id, 'recording-name', '\${GUAC_USERNAME}-\${GUAC_DATE}-\${GUAC_TIME}'),
            ($id, 'recording-include-keys', 'true');
    "

    # create sharing profiles Watch and Collaborate
    _mariadb "
        INSERT INTO guacamole_sharing_profile
            (sharing_profile_name, primary_connection_id)
        VALUES
            ('Watch', $id),
            ('Collaborate', $id);
    "

    # add the parameter read-only for the Watch sharing profile
    local id_watch=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = '$id' AND sharing_profile_name = 'Watch';
    ")
    _mariadb "
        INSERT INTO guacamole_sharing_profile_parameter
        VALUES ($id_watch, 'read-only', 'true');
    "
}

_guac_conn_list() {
    _mariadb "SELECT connection_name FROM guacamole_connection"
}

_guac_conn_remove() {
    local conn=$1
    [[ -z $conn ]] && _guac_fail

    _mariadb "
        DELETE FROM guacamole_connection
        WHERE connection_name='$conn';
    "

    # parameters and sharing profiles will be deleted automatically by the database triggers
}

_guac_user_add() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    local password=$2
    [[ -z $password ]] && _guac_fail

    # check that it does not already exist
    local id=$(_mariadb "
        SELECT entity_id FROM guacamole_entity
        WHERE name = '$username' AND type = 'USER'
    ")
    [[ -n $id ]] && fail "User already exists"

    # create a user entity
    _mariadb "
        INSERT INTO guacamole_entity
        SET type = 'USER',
            name = '$username';
    "

    # insert the user
    local entity_id=$(_mariadb "
        SELECT entity_id FROM guacamole_entity 
        WHERE name = '$username' AND type = 'USER'
    ")
    _mariadb "
        SET @salt = UNHEX(SHA2(UUID(), 256));
        INSERT INTO guacamole_user
        SET entity_id = $entity_id,
            password_hash = UNHEX(SHA2(CONCAT('$password', HEX(@salt)), 256)),
            password_salt = @salt,
            password_date = NOW();
    "

    # set permission for this user to READ
    local user_id=$(_mariadb "
        SELECT user_id FROM guacamole_user 
        WHERE entity_id = '$entity_id'
    ")
    _mariadb "
        INSERT INTO guacamole_user_permission
        VALUES
            ($entity_id,$user_id,'READ');
    "    
}

_guac_user_list() {
    local username=$1

    if [[ -z $username ]]; then
        _mariadb "
            SELECT name
            FROM guacamole_entity
            NATURAL JOIN guacamole_user
            WHERE type = 'USER'
              AND name != '$ADMIN'
              AND full_name IS NULL;
        "
    else
        _mariadb "
            SELECT connection_name
            FROM guacamole_entity
            NATURAL JOIN guacamole_connection_permission
            NATURAL JOIN guacamole_connection
            WHERE name = '$username';
        "
    fi
}

_guac_user_remove() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    [[ $username == $ADMIN ]] && return

    _mariadb "
        DELETE FROM guacamole_entity
        WHERE name = '$username' AND type = 'USER';
    "

    # all the dependent records will be deleted automatically by the database
}

_guac_user_connect() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    local conn=$2
    [[ -z $conn ]] && _guac_fail

    # get the user id (or entity_id)
    local uid=$(_mariadb "
        SELECT entity_id FROM guacamole_entity 
        WHERE name = '$username' AND type = 'USER';
    ")
    [[ -n $uid ]] || fail "User '$username' does not exist."

    # get the connection id
    local conn_id=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$conn';
    ")
    [[ -n $conn_id ]] || fail "Connection '$conn' does not exist."

    # get the sharing profile ids for this connection
    local watch_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $conn_id
          AND sharing_profile_name = 'Watch';
    ")
    local collab_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $conn_id
          AND sharing_profile_name = 'Collaborate';
    ")

    # set connection permissions and sharing profile permissions to READ
    _mariadb "
        INSERT INTO guacamole_connection_permission
        VALUES ($uid, $conn_id, 'READ');

        INSERT INTO guacamole_sharing_profile_permission
        VALUES
            ($uid, $watch_pid, 'READ'),
            ($uid, $collab_pid, 'READ');
    "
}

_guac_user_disconnect() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    local conn=$2
    [[ -z $conn ]] && _guac_fail

    # get the user id (or entity_id)
    local uid=$(_mariadb "
        SELECT entity_id FROM guacamole_entity
        WHERE name = '$username' AND type = 'USER';
    ")
    [[ -n $uid ]] || fail "User '$username' does not exist."

    # get the connection id
    local conn_id=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$conn';
    ")
    [[ -n $conn_id ]] || fail "Connection '$conn' does not exist."

    # get the sharing profile ids for this connection
    local watch_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $conn_id
          AND sharing_profile_name = 'Watch';
    ")
    local collab_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $conn_id
          AND sharing_profile_name = 'Collaborate';
    ")

    # delete connection permissions and sharing profile permissions
    _mariadb "
        DELETE FROM guacamole_connection_permission
        WHERE entity_id = $uid AND connection_id = $conn_id;

        DELETE FROM guacamole_sharing_profile_permission
        WHERE entity_id = $uid AND sharing_profile_id = $watch_pid;

        DELETE FROM guacamole_sharing_profile_permission
        WHERE entity_id = $uid AND sharing_profile_id = $collab_pid;
    "
}

_guac_guest_add() {
    local conn=$1
    [[ -z $conn ]] && _guac_fail
    echo $conn | grep -qE ':guest(:|$)' || fail "'$conn' is not a guest connection"

    local username=$2
    [[ -z $username ]] && _guac_fail
    local password=$3
    [[ -z $password ]] && password=$username

    # test that connection exists
    local conn_id=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$conn';
    ")
    [[ -n $conn_id ]] || fail "Connection '$conn' does not exist."

    # add user
    _guac_user_add $username $password
    _guac_user_connect $username $conn

    # set full_name to 'Guest'
    # this is how we distinguish a guest user from a normal user
    local uid=$(_mariadb "
        SELECT entity_id FROM guacamole_entity 
        WHERE name = '$username' AND type = 'USER'
    ")
    _mariadb "
        UPDATE guacamole_user
        SET full_name = 'Guest'
        WHERE entity_id = $uid;
    "
}

_guac_guest_remove() {
    local conn=$1
    [[ -z $conn ]] && _guac_fail
    local username=$2

    local match_username=''
    [[ -n $username ]] && match_username=" AND name = '$username'"
    _mariadb "
        CREATE TEMPORARY TABLE remove_list AS
            SELECT entity_id
            FROM guacamole_connection_permission
            NATURAL JOIN guacamole_connection
            NATURAL JOIN guacamole_entity
            NATURAL JOIN guacamole_user
            WHERE connection_name = '$conn'
              AND type = 'USER'
              AND full_name = 'Guest'
              $match_username
              ;

        DELETE FROM guacamole_entity
        WHERE entity_id IN 
              (SELECT entity_id FROM remove_list);
    "

    # all the dependent records will be deleted automatically by the database
}

_guac_guest_list() {
    _mariadb "
        SELECT connection_name, name
        FROM guacamole_entity
        NATURAL JOIN guacamole_user
        NATURAL JOIN guacamole_connection_permission
        NATURAL JOIN guacamole_connection
        WHERE name != '$ADMIN'
          AND type = 'USER'
          AND full_name = 'Guest';
    "
}
