cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # dump the content of the database
    if [[ -z $DBHOST ]]; then
        local dbname=${DOMAIN//./_}
        dbname=${dbname:-guacamole_db}
        local dump="mysqldump --allow-keywords --opt"
        ds exec $dump $dbname > $backup/$dbname.sql
    else
        local dump="mysqldump -h $DBHOST -P $DBPORT -u $DBUSER --password=$DBPASS --allow-keywords --opt"
        ds exec $dump $DBNAME > $backup/$DBNAME.sql
    fi

    # backup settings
    cp settings.sh $backup/

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
}
