cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod +x .
    mkdir -p recordings
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/recordings,dst=/var/lib/guacamole/recordings \
	"$@"
    _create_cmd_guac
}

_create_cmd_guac() {
    local guacdir=$(basename $(pwd))
    mkdir -p $DSDIR/cmd/
    cat <<-__EOF__ > $DSDIR/cmd/guac.sh
cmd_guac() {
    case \$1 in
        url)   echo "https://$DOMAIN" ;;
        *)     ds @$guacdir guac "\$@" ;;
    esac
}
__EOF__
}
