include(bookworm)

RUN <<EOF
  apt update
  apt upgrade --yes
  apt install --yes \
      nginx ssl-cert wget xmlstarlet

  # install tomcat9 from debian11
  echo "deb http://deb.debian.org/debian/ bullseye main" > /etc/apt/sources.list.d/bullseye.list
  apt update
  apt install --yes \
      tomcat9 tomcat9-admin tomcat9-common tomcat9-user
  sed -i 's/^/#/' /etc/apt/sources.list.d/bullseye.list
  apt update

  # install Guacamole dependences
  apt install --yes \
      build-essential libcairo2-dev libjpeg62-turbo-dev libpng-dev libtool-bin uuid-dev
  apt install --yes \
      libavcodec-dev libavformat-dev libavutil-dev libswscale-dev \
      freerdp2-dev libpango1.0-dev libssh2-1-dev libtelnet-dev libvncserver-dev libwebsockets-dev \
      libpulse-dev libssl-dev libvorbis-dev libwebp-dev

  VER=1.5.5
  ARCHIVE=http://archive.apache.org/dist/guacamole/$VER

  # install guacamole server
  wget "$ARCHIVE/source/guacamole-server-$VER.tar.gz"
  tar -xzf guacamole-server-$VER.tar.gz
  cd guacamole-server-*/
  CFLAGS=-Wno-error ./configure --with-systemd-dir=/etc/systemd/system/
  make
  make install
  ldconfig
  systemctl enable guacd
  cd ..
  rm -rf guacamole-server-$VER/
  rm guacamole-server-$VER.tar.gz

  # install guacamole client
  wget "$ARCHIVE/binary/guacamole-$VER.war"
  mkdir -p /etc/guacamole/
  mv guacamole-$VER.war /etc/guacamole/guacamole.war
  ln -sf /etc/guacamole/guacamole.war /var/lib/tomcat9/webapps/

  # install mariadb 10.11.7
  apt install --yes \
      curl ca-certificates apt-transport-https
  curl -LO https://r.mariadb.com/downloads/mariadb_repo_setup
  chmod +x mariadb_repo_setup
  ./mariadb_repo_setup --mariadb-server-version="mariadb-10.11.7" --skip-maxscale
  rm mariadb_repo_setup
  apt install --yes \
      mariadb-server \
      mariadb-client \
      mariadb-backup \
      mariadb-common \
      libmariadb-java \
      mariadb-plugin-provider-bzip2 \
      mariadb-plugin-provider-lz4 \
      mariadb-plugin-provider-lzma \
      mariadb-plugin-provider-lzo \
      mariadb-plugin-provider-snappy
  mkdir -p /etc/guacamole/lib
  ln -sf /usr/share/java/mariadb-java-client.jar /etc/guacamole/lib/

  # install font Monaco
  mkdir -p /usr/share/fonts/truetype/Monaco
  cd /usr/share/fonts/truetype/Monaco
  wget https://github.com/mikesmullin/gvim/raw/master/.fonts/Monaco_Linux.ttf
  fc-cache -f .

  # install the auth-jdbc extension
  EXTENSION=guacamole-auth-jdbc-$VER
  wget "$ARCHIVE/binary/$EXTENSION.tar.gz"
  tar -xzf $EXTENSION.tar.gz
  mkdir -p /etc/guacamole/extensions/
  cp $EXTENSION/mysql/guacamole-auth-jdbc-mysql-$VER.jar /etc/guacamole/extensions/
  cp $EXTENSION/mysql/schema/*.sql /etc/guacamole/
  rm $EXTENSION.tar.gz
  rm -rf $EXTENSION/

  # install the history-recording-storage extension
  EXTENSION=guacamole-history-recording-storage-$VER
  wget "$ARCHIVE/binary/$EXTENSION.tar.gz"
  tar -xzf $EXTENSION.tar.gz
  mkdir -p /etc/guacamole/extensions/
  cp $EXTENSION/$EXTENSION.jar /etc/guacamole/extensions/
  rm $EXTENSION.tar.gz
  rm -rf $EXTENSION/
EOF
